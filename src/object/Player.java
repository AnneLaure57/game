package object;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;

public class Player<Card> {
	
	private String name;
	private int age;
	//Deck<Card> -> ArrayList<Card>
	private ArrayList<Card> deckPlayer;
	private HashMap<Integer, ArrayList>  mainJoueur;
	private int nbErreurs;
	
	public Player(int nbErreurs){
		super();
		this.nbErreurs = nbErreurs;
	}
	
	public Player(String name, int age, ArrayList<Card> deckPlayer) {
		super();
		this.name = name;
		this.age = age;
		this.deckPlayer = deckPlayer;
	}

	public Player(String name, int age, HashMap<Integer, ArrayList> mainJoueur) {
		// TODO Auto-generated constructor stub
		super();
		this.name = name;
		this.age = age;
		this.mainJoueur = mainJoueur;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public HashMap<Integer, ArrayList>  getMainJoueur() {
		return mainJoueur;
	}

	public void setMainJoueur(HashMap<Integer, ArrayList>  mainJoueur) {
		this.mainJoueur = mainJoueur;
	}
	
	public ArrayList<Card> getDeckPlayer() {
		return deckPlayer;
	}

	public void setDeckPlayer(ArrayList<Card> deckPlayer) {
		this.deckPlayer = deckPlayer;
	}
	
    //Comparer les joueurs par age
    public int compare(Player p, Player p2) 
    { 
        return p.age - p2.age; 
    }
    
	public int getNbErreurs() {
		return nbErreurs;
	}

	public void setNbErreurs(int nbErreurs) {
		this.nbErreurs = nbErreurs;
	}

	@Override
	//Si pb affichage modifier le TpString
	public String toString() {
		return "Player [name=" + name + ", age=" + age + ",\n mainJoueur=" + deckPlayer + "]";
	}

}


	  
    
