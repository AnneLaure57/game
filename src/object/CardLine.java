package object;

public class CardLine extends Card{
	
	private int population;
	private double superficie;
	private double polution;
	private int pib;
	private String frontPicture;
	private String backPicture;

	public CardLine(String name, String namePicture) {
		super(name, namePicture);
		// TODO Auto-generated constructor stub
	}
	
	public CardLine(String name, int population, double superficie, double polution, int pib , String namePicture) {
		super(name, namePicture);
		// TODO Auto-generated constructor stub
		this.population = population;
		this.superficie = superficie;
		this.polution = polution;
		this.pib=pib;
	}
	
	

	public CardLine(String name, String namePicture, int population, double superficie, double polution, int pib,
			String frontPicture, String backPicture) {
		super(name, namePicture);
		this.population = population;
		this.superficie = superficie;
		this.polution = polution;
		this.pib = pib;
		this.frontPicture = namePicture+".jpeg";
		this.backPicture = namePicture+"_reponse.jpeg";
	}

	public int getPopulation() {
		return population;
	}

	public void setPopulation(int populaStion) {
		this.population = population;
	}

	public double getSuperficie() {
		return superficie;
	}

	public void setSuperficie(double superficie) {
		this.superficie = superficie;
	}

	public double getPolution() {
		return polution;
	}

	public void setPolution(double polution) {
		this.polution = polution;
	}

	public int getPib() {
		return pib;
	}

	public void setPib(int pib) {
		this.pib = pib;
	}
    
	public String getFrontPicture() {
		return frontPicture;
	}

	public void setFrontPicture(String frontPicture) {
		this.frontPicture = frontPicture;
	}

	public String getBackPicture() {
		return backPicture;
	}

	public void setBackPicture(String backPicture) {
		this.backPicture = backPicture;
	}

	@Override
	public String toString() {
		return "CardLine [population=" + population + ", superficie=" + superficie + ", polution=" + polution + ", pib="
				+ pib + ", Name=" + getName() + ", Picture=" + getNamePicture() + "]\n";
	}

}
