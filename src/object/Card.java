package object;

public class Card {
	
	//Correspond soit � la ville (Globetrotters) soit � l'invention (TimeLine)
	private String name;
	
	private String namePicture;

	public Card(String name, String namePicture) {
		super();
		this.name = name;
		this.namePicture = namePicture;

	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNamePicture() {
		return namePicture;
	}

	public void setNamePicture(String namePicture) {
		this.namePicture = namePicture;
	}

}
