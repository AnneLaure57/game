package object;

public class Bot <Card>{
	
	private String name;
	private Deck <Card> mainBot;
	
	public Bot(String name, Deck<Card> mainBot) {
		super();
		this.name = name;
		this.mainBot = mainBot;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Deck<Card> getMainBot() {
		return mainBot;
	}

	public void setMainBot(Deck<Card> mainBot) {
		this.mainBot = mainBot;
	}

	@Override
	public String toString() {
		return "Bot [name=" + name + ", mainBot=" + mainBot + "]";
	}
	
	
}
