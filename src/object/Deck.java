package object;

import java.util.ArrayList;

public class Deck<Card>{
	
	private ArrayList<Card> Card;

	public Deck(ArrayList<Card> card) {
		super();
		Card = card;
	}

	public ArrayList<Card> getCard() {
		return Card;
	}

	public void setCard(ArrayList<Card> card) {
		Card = card;
	}

	@Override
	public String toString() {
		return "Deck [" + Card + "]";
	}
	
}
