package object;

public class CardTimeLine extends Card{
	
	private int date;
	
	private String frontPicture;
	
	private String backPicture;
	
	public CardTimeLine(String name, String namePicture) {
		super(name, namePicture);
		// TODO Auto-generated constructor stub
	}
	
	public CardTimeLine(String name, int date, String namePicture) {
		super(name, namePicture);
		this.date=date;
	}
	
	//Pour l'interface
	public CardTimeLine(String name, String namePicture, int date, String frontPicture, String backPicture) {
		super(name, namePicture);
		this.date = date;
		this.frontPicture = namePicture+".jpeg";
		this.backPicture = namePicture+"_date.jpeg";
	}

	public int getDate() {
		return date;
	}

	public void setDate(int date) {
		this.date = date;
	}

	public String getFrontPicture() {
		return frontPicture;
	}

	public void setFrontPicture(String frontPicture) {
		this.frontPicture = frontPicture;
	}

	public String getBackPicture() {
		return backPicture;
	}

	public void setBackPicture(String backPicture) {
		this.backPicture = backPicture;
	}

	@Override
	public String toString() {
		//On r�cup�re le nom de l'image avec le getter getNamePicture
		return "CardTimeLine [ Invention=" + getName() + ", date=" + date + ", Nom image=" + getNamePicture()
				+ "]\n";
	}
	
}
