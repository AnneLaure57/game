package main;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

import Interface.Menu;
import methods.Play;
import methods.Reading;
import object.Card;
import object.CardTimeLine;


public class Main {

	public static void main(String[] args) {
		// TODO Mettre en place les threads
		
		//S�lection du jeu 
		Play p = new Play();
		//Choix jeu
		p.choixJ();
		Scanner sc = new Scanner(System.in);
		//fonction jeu
		while(!sc.hasNextInt())
		{
			System.out.println("Veuillez saisir un nombre :");
			sc.next();
		}
		int choice = sc.nextInt();
		p.start(choice);
	}

}
