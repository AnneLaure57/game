package methods;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import object.Card;
import object.CardLine;
import object.CardTimeLine;
import object.Player;
import java.util.HashMap;
import java.util.Random;
import java.util.Scanner; 


public class Play {
		
		int nbTours =0;
		int score=0;
		int nb;
		private Scanner sc;
	
	    //affichage choix jeu
		public void choixJ(){
			System.out.println("Bienvenue � vous,");
			System.out.println("choissisez un jeu :");
			System.out.println("1-TimeLine");
			System.out.println("2-CardLine");
			//TODO reprendre partie en cours
		}
		
		//Choix jeu (TimeLine ou CardLine)
		public void start(int choixJ)
		{
			if(choixJ == 1){
				//Lecture fichier TimeLine
				Reading readT = new Reading();
				readT.readTimeline();
			}
			else if(choixJ == 2){
				//Lecture fichier CardLine
				Reading readC = new Reading();
				readC.readCardline();
			}
		}
	
	//M�langer les cartes du paquet
	public ArrayList<Card> melanger(ArrayList<Card> deckDebut){
		System.out.println("Fonction m�langer");
		ArrayList<Card> nvDeck = new ArrayList<Card>(deckDebut);
		Collections.shuffle(deckDebut);
		return nvDeck;
	}
	
	//Distributon des cartes pour les joueurs
	public void distributionCarte(int nbPlayers, ArrayList<Card> paquet)
	{
		int i = 0;
		ArrayList<Card> cartesJ = null ;
		ArrayList players = new ArrayList();
		//Pour r�cup�rer la main en fonction du joueur
		HashMap<Integer,ArrayList> hp = new HashMap<Integer,ArrayList>();
		System.out.println(nbPlayers);		
		if (nbPlayers >= 2 && nbPlayers <= 4)
		{
			System.out.println("il y a "+nbPlayers+" Joueurs");
			//R�cup�rer le nom du joueur
			System.out.println("Taille du paquet: "+paquet.size());
			//On distribue 6 cartes
			for (i=1; i < nbPlayers+1; i++)
			{
				//Cr�er la main du joueur courant
				ArrayList<Card> handPlayer = new ArrayList<Card>();
				for(int j=0; j<6; j++){
					handPlayer.add((Card) paquet.get(j));
					//System.out.println("Carte en main Joueur n� "+i+": "+handPlayer.get(j));
					//Les enlever du paquet initial
					paquet.remove(j);
				}
				//ajoute la main player au joueur
				players.add(handPlayer);
			}
		}
		else if (nbPlayers > 4 && nbPlayers <= 6)
		{
			System.out.println("il y a "+nbPlayers+" Joueurs");
			//R�cup�rer le nom du joueur
			//On distribue 5 cartes
			for (i=1; i < nbPlayers+1; i++)
			{
				//Cr�er la main du joueur courant
				ArrayList<Card> handPlayer = new ArrayList<Card>();
				for(int j=0; j<5; j++){
					handPlayer.add((Card) paquet.get(j));
					//System.out.println("Carte en main Joueur n� "+i+": "+handPlayer.get(j));
					paquet.remove(j);
				}
				//ajoute la main player au joueur
				players.add(handPlayer);
			}
		}
		else if (nbPlayers > 6 && nbPlayers <= 8)
		{
			System.out.println("il y a "+nbPlayers+" Joueurs");
			//R�cup�rer le nom du joueur
			//On distribue 4 cartes
			for (i=1; i < nbPlayers+1; i++)
			{
				//Cr�er la main du joueur courant
				ArrayList<Card> handPlayer = new ArrayList<Card>();
				for(int j=0; j<4; j++){
					handPlayer.add((Card) paquet.get(j));
					//System.out.println("Carte en main Joueur n� "+i+": "+handPlayer.get(j));
					paquet.remove(j);
				}
				//ajoute la main player au joueur
				players.add(handPlayer);
			}
		}
		else if (nbPlayers < 2 || nbPlayers >= 9)
		{
			System.out.println("C'est impossible de faire une distribution!");
			System.exit(0);
		}
		for (i=0; i< players.size(); i++)
		{
			//affiche la main du joueur
			cartesJ = (ArrayList<Card>) players.get(i);
			//System.out.println("Main du joueur n� "+i+" :\n"+cartesJ);
			//On ajoute � la liste pour avoir le couple num joueur/main
			hp.put(i, cartesJ);
			//System.out.println("Main du joueur n� "+i+" :\n"+cartesJ+"HM");
		}
		//Fonction premier joueur commence
		begin(hp,paquet);
	}
	
	//Association Joueur -> HM num joueur + mainJoueur
	public void begin(HashMap<Integer, ArrayList> hp, ArrayList<Card> paquet)
	{
		//Premier joueur en fonction de l'age
		//Poser carte JoueurCourant -> Board
		//Changement de joueur
		for(int i=0; i<hp.size();i++)
		{
			System.out.println("Main du joueur n� "+i+" :\n"+hp.get(i)+"de la HM");
		}
		
		ArrayList list = new ArrayList();
		//TODO � corriger
		//Provenance du probl�me inversion main entre joueurs
		//Demande du nom et �ge du J1
		System.out.println("Saisir le nom du joueur J1: ");
		Scanner scn = new Scanner(System.in);
		String name = scn.nextLine();
		System.out.println("Saisir l'�ge du joueur J1: ");
		Scanner sca = new Scanner(System.in);
		while(!sca.hasNextInt())
		{
			System.out.println("Veuillez saisir un nombre :");
			sca.next();
		}
		int age = sca.nextInt();
		
		//Demande du nom et �ge du J2
		System.out.println("Saisir le nom du joueur J2: ");
		Scanner scn2 = new Scanner(System.in);
		String name2 = scn.nextLine();
		System.out.println("Saisir l'�ge du joueur J2: ");
		Scanner sca2 = new Scanner(System.in); 
		while(!sca2.hasNextInt())
		{
			System.out.println("Veuillez saisir un nombre :");
			sca2.next();
		}
		int age2 = sca2.nextInt();
		
		
		//Si les deux joueurs ont le m�me �ge
		//Il prend le premier joueur ayant saisie son �ge en premier comme J1
		Player p1 = new Player(name,age, hp.get(1));
		Player p2 = new Player(name2, age2, hp.get(0));
		
		list.add(p1);
		list.add(p2);
		
		//Tri des joueurs par rapport � l'�ge
		Collections.sort(list, new comparator());
		
		for(int j=0;j<list.size();j++)
		{
			System.out.println(list.get(j));
		}
		//le joueur num 0 joue car c'est le plus jeune
		//gr�ce au tri
		Object firstPlayer = list.get(0);
		//On r�cup�re la main du joueur 0
		ArrayList hand = hp.get(0);
		System.out.println("Premier joueur � jouer :"+firstPlayer);
		
		//Iniatilisation du plateau (contient au moins une carte)
		//avant que le premier joueur pose une carte de sa main
		ArrayList<Card> board = new ArrayList<Card>();
		for(int j=0; j<1; j++)
		{
			//On ajoute la carte au tableau
			board.add((Card) paquet.get(j));
			//On supprime cette carte du paquet
			paquet.remove(j);
			System.out.println("Initialisation board :"+board);
		}
		poserCarte((Player) firstPlayer,hp, hand, list, board, paquet);
	}

	//Du paquet vers le plateau
	//TODO V�rifications date pour timeLine pour le bot
	//TODO V�rifs valeur s�lectionn�e par le joueur(superficie, population etc) pour CL 
	public void poserCarte(Player firstPlayer,HashMap<Integer, ArrayList> hp, ArrayList<Card> hand,
			ArrayList list,ArrayList<Card> board, ArrayList<Card> paquet)
	{
		//On retirer al�atoirement une carte
		//TODO Temporaire en attendant l'interface
		//TODO syst�me point joueur
		//TODO si erreur, le joueur pioche
		//Random random = new Random();
		for(int j=0; j<1; j++)
		{
			//r�cup�re al�atoirement une carte de la main du joueur
			//int nbAlea = random.nextInt(hand.size()-1);
			System.out.println("Saisir une carte de votre main: ");
			Scanner sc = new Scanner(System.in);
			while(!sc.hasNextInt())
			{
				System.out.println("Veuillez saisir un nombre :");
				sc.next();
			}
			int nbAlea = sc.nextInt();
			if(nbAlea <= 5)
			{
				//On ajoute la carte de la main au plateau
				board.add((Card) hand.get(nbAlea));
				//on la retire de la main du joueur
				hand.remove(nbAlea);
			}
			else{
				System.out.println("Veuillez saisir un autre nombre !");
				poserCarte((Player) firstPlayer,hp, hand, list, board, paquet);
			}
			
		}
		//Affichage du plateau
		System.out.println("Affichage Plateau: "+board);
		//Affichage de la nouvelle main du joueur
		System.out.println("Affichage nv Main: "+hand);
		//TODO � corriger
		Player newPlayer = new Player(firstPlayer.getName(),firstPlayer.getAge(),firstPlayer.getMainJoueur());
		//list.add(newPlayer);
		//On ajoute les infos du joueur 0
		System.out.println("Actualisation:"+list.get(0));
		//System.out.println("Nouvelle Liste :"+list.get(1));
		mainVide((ArrayList<Card>) hand, (Player) newPlayer);
		changementJoueur(list,hp,hand,board,0,paquet);
		
	}
	
	private void poserCarteJS(Player newPlayer, HashMap<Integer, ArrayList> hp, ArrayList<Card> hand,
			ArrayList listPlayers, ArrayList<Card> board,int i,ArrayList<Card> paquet) {
		
		hand = hp.get(i);
		int nbAlea=0;
		for(int j=0; j<1; j++)
		{
			System.out.println("Saisir une carte de votre main: ");
			Scanner sc = new Scanner(System.in);
			while(!sc.hasNextInt())
			{
				System.out.println("Veuillez saisir un nombre :");
				sc.next();
			}
			nbAlea = sc.nextInt();
			if(nbAlea <= 5)
			{
				board.add((Card) hand.get(nbAlea));
				//on la retire de la main du joueur
				hand.remove(nbAlea);
			}
			else{
				System.out.println("Veuillez saisir un autre nombre !");
				poserCarteJS((Player) newPlayer, hp, hand, listPlayers, board, j,paquet);
			}
		}
		//Affichage du plateau
		System.out.println("Affichage Plateau: "+board);
		//Affichage de la nouvelle main du joueur
		System.out.println("Affichage nv Main: "+hand);
		//TODO � corriger
		Player nextPlayer = new Player(newPlayer.getName(),newPlayer.getAge(),newPlayer.getMainJoueur());
		//listPlayers.add(newPlayer);
		//On ajoute les infos du joueur 0
		System.out.println("Actualisation main joueur : "+listPlayers.get(i));
		mainVide((ArrayList<Card>) hand, (Player) newPlayer);
		//System.out.println("Nouvelle Liste :"+list.get(1));
		
		//CardLine
		//affichageCardLine();
		
		//On compare si la carte est pos� au bonne endroit
		//TODO TESTS
		//TODO � d�cocher pour tester
		//pour PIOCHE et DEFAUSSE, �a marche
		if (ComparaisonTimeLine((Card) hand.get(nbAlea),board) == false) {
		
			//si c'est faute
			System.out.println("fonction d�fausse");
			//On enl�ve la carte du plateau
			Card carteDefausse = (Card) hand.get(nbAlea);
			System.out.println("Carte: "+carteDefausse);
			defausse(paquet,board,carteDefausse);
			
			//TODO compteur erreur du joueur +1
			nb = newPlayer.getNbErreurs()+1;
			newPlayer.setNbErreurs(nb);
			System.out.println("Nombre d'erreurs "+nb);
			
			System.out.println("fonction pioche");
			ArrayList<Card> bucket = null;
			//le joueur qui s'est tromp� pioche une carte
			//On v�rifie s'il y a un paquet piocher
			//Si le paquet est vide, la poubelle devient le nv paquet de pioche
			pioche(newPlayer,hand,listPlayers,hp,paquet,bucket);
		}
		//Changement joueur
		nbTours = nbTours+1;
		System.out.println("Tour n�"+nbTours);
		changementJoueur(listPlayers,hp,hand,board,i, paquet);
	}
	
	// Comparaison des cartes 
	public boolean ComparaisonTimeLine(Card carte1, ArrayList<Card> board ) {
		System.out.println("Affichage du plateu ");

		int i = 0;
		for(i=0; i<board.size();i++)
		{
			System.out.println("Valeur de i : "+i+", carte: "+board.get(i));
		}
		//int i = board.indexOf(carte1);
		CardTimeLine carte = (CardTimeLine) carte1;
		System.out.println("Avant cast: "+i);
		int a = i-2;
		int b= i-1;
		CardTimeLine carteAvant = (CardTimeLine) board.get(a);
		//CardTimeLine carteApres = (CardTimeLine) board.get(b);
		//null car dans une arraylist, le nv element est ajout� � la fin , donc
		//pas de carteApres
		CardTimeLine carteApres = null;
		
		System.out.println("Apres cast");
		
		if (carteApres == null) {
			//si la date de la carte est plus grande ou �gale que la carte avant
			if(carte.getDate() >= carteAvant.getDate()) {
				System.out.println("Compare Carteavant");
				return true;
			
			}
		}else 
		{
			//Si il n'y a pas de carte avant 
			if(carteAvant == null) {
				//si la date de la carte est plus petite ou �gale que la carte apr�s
				if(carte.getDate() <= carteApres.getDate()) {
					System.out.println("Compare carteApres");
					return true;
				}
			}else {
				//si la date de la carte est plus petite ou �gale que la carte apr�s
				//si la date de la carte est plus grande ou �gale que la carte avant
				if(carte.getDate() <= carteApres.getDate() && carte.getDate() >= carteAvant.getDate()) {
					System.out.println("Compare avec CarteAvant et CarteApres");
					return true;
				}
			}
			
		}
		System.out.println("fin de la fonction");
		return false ;
	}
	
	//Changement de tour, on passe au joueur suivant de la liste
	public void changementJoueur(ArrayList listPlayers,HashMap<Integer, ArrayList> hp, 
			ArrayList<Card> hand,ArrayList<Card> board, int j, ArrayList<Card> paquet)
	{
		if (j == (listPlayers.size()-1)) {
			j=0;
		} else {
			j++;
		}
		//On r�cup�re le newJoueur et sa main
		Object newPlayer = listPlayers.get(j);
		Object newHand = hp.get(j);
		System.out.println("Joueur suivant: "+newPlayer);
		System.out.println("Main du joueur suivant: "+newHand);
		mainVide((ArrayList<Card>) newHand, (Player) newPlayer);
		poserCarteJS((Player) newPlayer, hp, hand, listPlayers, board, j,paquet);
	}

	//PiocherCarte pour joueur
	public void pioche(Player playerF, ArrayList<Card> hand,ArrayList listPlayers,
			HashMap<Integer, ArrayList> hp, ArrayList<Card> paquet,ArrayList<Card> bucket)
	{
		//TODO checker si on peut ajouter une carte en fonction de la taille de la main
		//Le joueur pioche s'il s'est tromp�
		System.out.println("Joueur ayant fait l'erreur"+playerF);
		if(paquet.isEmpty() || paquet.size() >=1){
			if(hand.size()>=6){
				for(int j=0; j<1; j++)
				{
					//Ajoute al�atoirement la carte dans la main du joueur
					//Qui a fait la faute
					hand.add((Card) paquet.get(j));
					//On supprimer la carte du paquet
					paquet.remove(j);			
				}
				System.out.println("la nouvelle main"+hand);
			}
		}
		else{
			changementPaquet(paquet,bucket);
		}
	}
	
	//Si la main des joueurs est vide
	public void mainVide(ArrayList<Card> hand, Player newPlayer)
	{
		//System.out.println("Main du joueur " +hand);
		if(hand.isEmpty() || hand.size() == 1)
		{
			System.out.println("dernier �l�ment :"+hand);
			hand.remove(0);
			hand.clear();
			scoreJoueur(newPlayer, nbTours);
		}
	}
	
	//Suppression -> Jeter carte si erreur
	//Carte retourne dans la corbeille -> d�fausse
	public void defausse(ArrayList<Card> board,ArrayList<Card> bucket, Card cardBucket)
	{
		bucket.add(cardBucket);
		//on la retire de la main du joueur
		board.remove(cardBucket);
		
	}
	
	//si paquet vide -> d�fausse = new paquet
	public void changementPaquet(ArrayList<Card> paquet,ArrayList<Card> bucket)
	{
		System.out.println("fonction changementPaquet");
		ArrayList<Card> newPaquet = new ArrayList<Card>();
		//Si le paquet est vide ou si la taille du paquet 
		if(paquet.isEmpty() || paquet.size() == 1)
		{
			System.out.println("Le paquet est vide");
			//Le nouveau paquet r�cup�re les cartes de la d�fausse
			newPaquet = bucket;
		}
		else
		{
			System.out.println("le paquet n'est pas vide");
		}
		System.out.println("Nouveau paquet :"+newPaquet);
	}
	
	public void scoreJoueur(Player winner, int nbTours)
	{
		//TODO Compter nb tours
		//TODO Compter nb fautes
		//TODO Score = nbTours * 100 / nbErreurs;
		score = nbTours * 1000 / nb;
		System.out.println("Fin de la partie, le joueur "+winner.getName()+" a gagn� !");
		System.out.println("Nombre total de tours: "+nbTours);
		System.out.println("Le score du joueur est de : "+score);
		System.out.println("Le nombre de fautes faite par le joueur: "+nb);
		System.out.println("Merci d'avoir jouer !\n");
		//TODO demander si on veut sauvegarder
		System.exit(1);
		rejouer();
	}
	
	public void rejouer(){
		System.out.println("Que voulez-vous faire");
		System.out.println("1-Rejouer");
		System.out.println("2-Sauvegarder TimeLine");
		System.out.println("3-Sauvegarder TimeLine");
		System.out.println("4-Quitter");
		sc = new Scanner(System.in);
		String reponse = sc.nextLine();
		int rep = Integer.parseInt(reponse);
		
		switch(rep)
		{
			case 1 :
				choixJ();
				break;
			case 2 :
				choixSauvTimeLine();
				break;
			case 3 :
				choixSauvCardLine();
				break;
			case 4 :
				System.out.println("Vous avez quitt� la plateforme !");
				System.exit(0);
				break;
		}
		
	}
	
	//sauvegarder la partie TimeLine
	public void choixSauvTimeLine(){
		Save s = new Save();
		//s.serialiserTimeLine();
	}
	
	//r�cup�rer les infos de la partie TimeLine
	public void choixPartieTimeLine(){
		Save s = new Save();
		//s.deserialiserTimeLine();
	}
	
	//r�cup�rer les infos de la partie CardLine
	public void choixSauvCardLine(){
		Save s = new Save();
		//s.serialiserTimeLine();
	}
	
	//r�cup�rer les infos de la partie CardLine
	public void choixPartieCardLine(){
		Save s = new Save();
		//s.deserialiserCardLine();
	}
////////////////////////////////CARDLINE/////////////////////////////////////////////////////
	//Fonction choix caract�ristiques de la partie
	public void affichageCardLine(ArrayList<Card> board){
		System.out.println("Quel base souhaitez-vous choisir :");
		System.out.println("	-1 : population");
		System.out.println("	-2 : superficie");
		System.out.println("	-3 : polution");
		System.out.println("	-4 : pib");
		Scanner sc = new Scanner(System.in);
		//fonction jeu
		while(!sc.hasNextInt())
		{
			System.out.println("Veuillez saisir un nombre :");
			sc.next();
		}
		int choice = sc.nextInt();
		choixCardline(choice,board);
	}
	
	public void choixCardline(int choix,ArrayList<Card> board){
		//Choix de la base pour le comparator entre collection
		//TODO r�organiser les cartes du Board en fonction de la base choisie
		ArrayList b = new ArrayList();
		b = board;
		switch(choix){
		case 1 : choix = 1;
				 System.out.println("Vous avez choisi le n�: "+ choix);
				 // Instance comparator population
				 comparatorCardLinePop cp = new comparatorCardLinePop();
				 //Tri des cartes
				 //TODO corriger les erreurs de Collections.sort
				 Collections.sort(b, cp);
				 for(int i=0;i<board.size();i++)
				 {
					 for(int j=1;j<board.size();j++)
					 {
						System.out.println(board.get(j));
					 }
				 }
				 break;
		case 2 : choix = 2;
		 		 System.out.println("Vous avez choisi le n�: "+choix);
		 		 //Instance comparator superficie
		 		 comparatorCardLineSup cs = new comparatorCardLineSup();
		 		 //Fonction comparator superficie;
		 		 //TODO corriger les erreurs de Collections.sort
				 Collections.sort(b, cs);;
				 for(int i=0;i<board.size();i++)
				 {
					 for(int j=1;j<board.size();j++)
					 {
						System.out.println(board.get(j));
					 }
				 }
		 		 break;
		case 3 : choix = 3;
		 		 System.out.println("Vous avez choisi le n�: "+choix);
		 		 //Instance comparator polution
		 		comparatorCardLinePol cpo = new comparatorCardLinePol();
		 		 //Fonction comparator polution;
		 		 //TODO corriger les erreurs de Collections.sort
				 Collections.sort(b, cpo);;
				 for(int i=0;i<board.size();i++)
				 {
					 for(int j=1;j<board.size();j++)
					 {
						System.out.println(board.get(j));
					 }
				 }
		 		 break;
		case 4 : choix = 4;
 				 System.out.println("Vous avez choisi le n�: "+choix);
 				 //Instance comparator polution
 		 		 comparatorCardLinePib cpi = new comparatorCardLinePib();
 				 //Fonction comparator polution;
 				 //TODO corriger les erreurs de Collections.sort
				 Collections.sort(b, cpi);;
				 for(int i=0;i<board.size();i++)
				 {
					 for(int j=1;j<board.size();j++)
					 {
						System.out.println(board.get(j));
					 }
				 }
 				 break;
 		default : System.out.println("Votre choix est invalide ");
 				 break;
		
		}

	}
	
}
