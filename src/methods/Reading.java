package methods;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

import object.Card;
import object.CardLine;
import object.CardTimeLine;

public class Reading {

	public void readTimeline(){
		
		Play jeu = new Play();
		//Menu menu = new Meu();
		
		//ArrayList de Cartes
		ArrayList<Card> Paquet = new ArrayList<Card>();
		
		FileReader monFichier = null;
		BufferedReader tampon = null;
		
		CardTimeLine Card = null;
		
		try{
			 //A adapter selon votre chemin
			//Pour r�cup�rer les cartes TimeLine
			 monFichier = new FileReader("C:\\Users\\Annelaure\\"
			 		+ "Desktop\\game\\data\\timeline\\timeline.csv");
			tampon = new BufferedReader(monFichier);
			
			int i = 1;
			
			while (true)
			{
				String ligne = tampon.readLine();
				if(ligne==null)
				{
					break;
				}
				//System.out.println(ligne);
				if( i > 1){
					//faire le split
					//pour �viter de lire la premi�re
					String values[] = ligne.split(";", -1);
					String InvObj = values[0];
					//convertir string to int Integer.ValueOf() ou Integer.parseint()
					String dateObj = values[1];//conversion
					String ImageObj = values[2];
					//Convertir la string en int
					int dateOb = Integer.parseInt(dateObj);
					Card = new CardTimeLine(InvObj, dateOb, ImageObj);
					Paquet.add(Card);
				}
				i++;
			}
			//les cartes qui ont �t� lu dans le CSV
			System.out.println(Paquet);
			
			//m�langer le deck
			jeu.melanger(Paquet);
			System.out.println(Paquet);
			System.out.println("Saisir le nb de joueur :");
			Scanner sc = new Scanner(System.in);
			while(!sc.hasNextInt())
			{
				System.out.println("Veuillez saisir un nombre de joueurs(2): ");
				sc.next();
			}
			int players = sc.nextInt();
			
			//Distribution des cartes entre les joueurs
			jeu.distributionCarte(players,Paquet);
			
			//fermeture du fichier
			tampon.close();
			monFichier.close();
			
		}
		catch(Exception e)
		{
			System.out.println("Erreur lors de la lecture : "+e.getMessage());
		}
	  }
	public void readCardline(){
		
		Play jeu = new Play();
		//Menu menu = new Meu();
		
		//ArrayList de Cartes
		ArrayList<Card> Paquet = new ArrayList<Card>();
		
		FileReader monFichier = null;
		BufferedReader tampon = null;
		
		CardLine Card = null;
		
		try{
			 //A adapter selon votre chemin
			//Pour r�cup�rer les cartes TimeLine
			 monFichier = new FileReader("C:\\Users\\Annelaure\\"
			 		+ "Desktop\\game\\data\\cardline\\cardline.csv");
			tampon = new BufferedReader(monFichier);
			
			int i = 1;
			
			while (true)
			{
				String ligne = tampon.readLine();
				if(ligne==null)
				{
					break;
				}
				//System.out.println(ligne);
				if( i > 1){
					//faire le split
					//pour �viter de lire la premi�re
					
					String values[] = ligne.split(";",-1);
					String ville = values[0];
					String superficie = values[1];
					String population = values[2];
					String pib = values[3];
					String polution = values[4];
					String image = values[5];

					//Convertir la string en int
					int pop = Integer.parseInt(population);
					int pi = Integer.parseInt(pib);
					double sup = Double.parseDouble(superficie);
					polution = polution.replaceAll(",", ".");
					double pol = Double.parseDouble(polution);
					
					/*NumberFormat format = NumberFormat.getCurrencyInstance(Locale.FRENCH);
			        String currency = format.format(pol);
			        double polu = Double.parseDouble(currency);*/
					
					Card = new CardLine(ville,pop, sup, pol, pi, image);
					Paquet.add(Card);
				}
				i++;
			}
			//les cartes qui ont �t� lu dans le CSV
			System.out.println(Paquet);
			
			//m�langer le deck
			jeu.melanger(Paquet);
			System.out.println(Paquet);
			System.out.println("Saisir le nb de joueur :");
			Scanner sc = new Scanner(System.in);
			while(!sc.hasNextInt())
			{
				System.out.println("Veuillez saisir un nombre de joueurs (2): ");
				sc.next();
			}
			int players = sc.nextInt();
			
			//Distribution des cartes entre les joueurs
			jeu.distributionCarte(players,Paquet);
			
			/*//Choix base cardline
			jeu.affichage();
			Scanner scC = new Scanner(System.in);
			String choice = scC.nextLine();
			int base = Integer.valueOf(choice);
			jeu.choixCardline(base);*/
			
			//fermeture du fichier
			tampon.close();
			monFichier.close();
			
		}
		catch(Exception e)
		{
			System.out.println("Erreur lors de la lecture : "+e.getMessage());
		}
	}
}
