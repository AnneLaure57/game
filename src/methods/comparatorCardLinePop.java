package methods;

import java.util.Comparator;

import object.CardLine;
import object.Player;

public class comparatorCardLinePop implements Comparator<CardLine> {
	//Comparer les cards en fonction de la population
	@Override
	public int compare(CardLine cp1, CardLine cp2) {
		return cp1.getPopulation()- cp2.getPopulation();
	} 
}
