package methods;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import object.Card;
import object.Player;

public class Save {
	//TODO Sauvegarder les parties pour TimeLine & CardLine
	//TODO Sauvegarder les scores
	public class Serialiser implements Serializable{
		
		Reading file = new Reading();
		//Liste des joueurs
		ArrayList p = new ArrayList();
		//Liste Cartes en main
		ArrayList<Card> handPlayer = new ArrayList<Card>();
		//HM de la main joueur -> Son numéro joueur + sa main (liste handplayer)
		HashMap<Integer,ArrayList> hp = new HashMap<Integer,ArrayList>();
		//Liste des scores
		ArrayList score = new ArrayList();
		public Serialiser() {
			
		}
		//Pour TimeLine
		public void serialiserTimeLine () throws IOException {
			int i=1;
			String ligne;
			FileOutputStream out = new FileOutputStream ("C:\\Users\\Annelaure\\"
			 		+ "Desktop\\game\\data\\timeline\\timeline_save.csv");
			ObjectOutputStream s= new ObjectOutputStream(out);
			
			//TODO corriger la sauvegarde
			//car reading TimeLine n'est pas de type arraylist
			//p = (ArrayList) file.readTimeline();
			//nandPlayer = (ArrayList<Card>) file.readTimeline();
			//hp = (HashMap<Integer,ArrayList>) file.readTimeline();
				
			s.writeObject(p);
			s.writeObject(handPlayer);
			s.writeObject(hp);
			s.writeObject(score);
			s.flush();
			s.close();
		
		}
		
		public void deserialiserTimeLine () throws IOException, ClassNotFoundException {
			
			FileInputStream in = new FileInputStream ("C:\\Users\\Annelaure\\"
			 		+ "Desktop\\game\\data\\timeline\\timeline_save.dat");
			ObjectInputStream s= new ObjectInputStream(in);
			
			ArrayList savePlayers = new ArrayList();
			ArrayList saveScore = new ArrayList();
			ArrayList<Card> saveHand= new ArrayList<Card>();
			HashMap<Integer,ArrayList> saveHp= new HashMap<Integer,ArrayList>();
			
			savePlayers=(ArrayList) s.readObject();
			saveHand=(ArrayList<Card>) s.readObject();
			saveHp=(HashMap<Integer,ArrayList>) s.readObject();
			saveScore=(ArrayList) s.readObject();
			
			s.close();
			in.close();
			//Afficher la liste des joueurs sauvegardés
			for (int i=0; i<savePlayers.size();i++)
			{
				p.get(i).toString();
					
			}
			//Afficher la liste des mains sauvegardés
			for (int i=0; i<saveHand.size();i++)
			{
				handPlayer.get(i).toString();
					
			}
			//Afficher la liste des joueurs+mains sauvegardés
			for (int i=0; i<saveHp.size();i++)
			{
				hp.get(i).toString();
					
			}
		}
		//Pour CardLine
		public void serialiserCardLine() throws IOException {
			int i=1;
			String ligne;
			FileOutputStream out = new FileOutputStream ("C:\\Users\\Annelaure\\"
			 		+ "Desktop\\game\\data\\cardline\\cardline_save.csv");
			ObjectOutputStream s= new ObjectOutputStream(out);
			
			//TODO corriger la sauvegarde
			//car reading TimeLine n'est pas de type arraylist
			//p = (ArrayList) file.readTimeline();
			//handPlayer = (ArrayList<Card>) file.readTimeline();
			//hp = (HashMap<Integer,ArrayList>) file.readTimeline();
				
			s.writeObject(p);
			s.writeObject(handPlayer);
			s.writeObject(hp);
			s.writeObject(score);
			s.flush();
			s.close();
		
		}
		
		public void deserialiserCardLine() throws IOException, ClassNotFoundException {
			
			FileInputStream in = new FileInputStream ("C:\\Users\\Annelaure\\"
			 		+ "Desktop\\game\\data\\cardline\\cardline_save.dat");
			ObjectInputStream s= new ObjectInputStream(in);
			
			ArrayList savePlayers = new ArrayList();
			ArrayList saveScore = new ArrayList();
			ArrayList<Card> saveHand= new ArrayList<Card>();
			HashMap<Integer,ArrayList> saveHp= new HashMap<Integer,ArrayList>();
			
			savePlayers=(ArrayList) s.readObject();
			saveHand=(ArrayList<Card>) s.readObject();
			saveHp=(HashMap<Integer,ArrayList>) s.readObject();
			saveScore=(ArrayList) s.readObject();
			
			s.close();
			in.close();
			//Afficher la liste des joueurs sauvegardés
			for (int i=0; i<savePlayers.size();i++)
			{
				p.get(i).toString();
					
			}
			//Afficher la liste des mains sauvegardés
			for (int i=0; i<saveHand.size();i++)
			{
				handPlayer.get(i).toString();
					
			}
			//Afficher la liste des joueurs+mains sauvegardés
			for (int i=0; i<saveHp.size();i++)
			{
				hp.get(i).toString();
					
			}
			//Afficher la liste des scores sauvegardés
			for (int i=0; i<saveScore.size();i++)
			{
				hp.get(i).toString();
					
			}
		}
		
	}

}
