package Interface;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Menu {

	public Menu () {
		//Initialisation de la fen�tre
		JFrame frame = new JFrame("TimeLine & CardsLine");
		frame.setSize(925,500);
		//Label qui s'affiche en haut 
		
		JLabel labelChoix = new JLabel("BIENVENUE");
		JPanel panelLabelChoix = new JPanel();
		// Ajustement de la taille du text dans label 
				labelChoix.setFont(new Font("Serif", Font.BOLD, 40));
		// MENU CHOIX DU JEU 
				//Inserer l'image de timeLine
				ImageIcon imageTime = new ImageIcon("src/Interface/timeline.jpg");
				JLabel imageTimeLine = new JLabel(imageTime);
				//Inserer l'image de cardLine
				ImageIcon imageCard = new ImageIcon("src/Interface/cardline.jpg");
				JLabel imageCardLine = new JLabel(imageCard);
				//LABEL 
				JLabel labelCard = new JLabel("            CARDLINE");
				JLabel labelTime = new JLabel("      TIMELINE");
				// Format des labels
				labelCard.setFont(new Font("Serif", Font.PLAIN, 20));
				labelTime.setFont(new Font("Serif", Font.PLAIN, 20));
				// Panel n�cessaire
				JPanel panelCard = new JPanel();
				JPanel panelTime = new JPanel();
				JPanel panelChoix = new JPanel();	
				JPanel panelMenuChoix = new JPanel();
				// panel pour les labels
				JPanel panelLabelTime = new JPanel();
				JPanel panelLabelCard = new JPanel();
				//remplissage des panelLabel
				panelLabelCard.add(labelCard);
				panelLabelTime.add(labelTime);
				//Organisation
				panelLabelCard.setLayout(new FlowLayout(FlowLayout.CENTER));
				panelLabelTime.setLayout(new FlowLayout(FlowLayout.CENTER));
				//Remplissage des panels 
				
				panelLabelChoix.add(labelChoix);
				panelCard.add(imageCardLine);
				panelCard.add(panelLabelCard);
				panelTime.add(imageTimeLine);
				panelTime.add(panelLabelTime);
				//Organisation des panels
				panelCard.setLayout(new BoxLayout(panelCard, BoxLayout.Y_AXIS));
				panelTime.setLayout(new BoxLayout(panelTime, BoxLayout.Y_AXIS));
				//Organisation et remplissage du panel Choix
				panelChoix.add(panelTime);
				panelChoix.add(panelCard);
				panelChoix.setLayout(new FlowLayout(FlowLayout.CENTER));
				panelLabelChoix.setLayout(new FlowLayout(FlowLayout.CENTER));
				//Organisation et remplissage du panel MENUCHOIX
				panelMenuChoix.add(panelLabelChoix);
				panelMenuChoix.add(panelChoix);
				panelMenuChoix.setLayout(new BoxLayout(panelMenuChoix, BoxLayout.Y_AXIS));
				// Appelle de formulaire d'information
				imageTimeLine.addMouseListener(new MouseListener() {
					
					@Override
					public void mouseReleased(MouseEvent arg0) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void mousePressed(MouseEvent arg0) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void mouseExited(MouseEvent arg0) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void mouseEntered(MouseEvent arg0) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void mouseClicked(MouseEvent arg0) {
						// TODO Auto-generated method stub
						System.out.println("TIME");
						InformationJoueur infos = new InformationJoueur(frame);
						
					}
				});
				
				imageCardLine.addMouseListener(new MouseListener() {
					
					@Override
					public void mouseReleased(MouseEvent arg0) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void mousePressed(MouseEvent arg0) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void mouseExited(MouseEvent arg0) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void mouseEntered(MouseEvent arg0) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void mouseClicked(MouseEvent arg0) {
						// TODO Auto-generated method stub
						System.out.println("Card");
					}
				});
				
				
		//MENU PRINCIPLE
		// d�claration des composant du menu 
		
		JButton bouton_jouer = new JButton ("Jouer");
		JButton bouton_option = new JButton ("Option");
		JButton bouton_quitter = new JButton ("Quitter");
		JPanel panel_menu = new JPanel();
		JLabel label = new JLabel("BIENVENUE");
		JPanel panel_jouer = new JPanel();
		JPanel panel_option = new JPanel();
		JPanel panel_quitter = new JPanel();
		JPanel panel_label = new JPanel();
		
		// Ajustement de la taille du text dans label 
		label.setFont(new Font("Serif", Font.BOLD, 40));
	
		//modification de la taille des bouton
		bouton_jouer.setPreferredSize(new Dimension(100,50));
		bouton_option.setPreferredSize(new Dimension(100,50));
		bouton_quitter.setPreferredSize(new Dimension(100,50));
		
		//ajout au panel 
		panel_label.add(label);
		panel_jouer.add(bouton_jouer);
		panel_option.add(bouton_option);
		panel_quitter.add(bouton_quitter);
		panel_menu.add(panel_label);
		panel_menu.add(panel_jouer);
		panel_menu.add(panel_option);
		panel_menu.add(panel_quitter);
		
		//Organiser les composants individuelement 
		panel_label.setLayout(new FlowLayout(FlowLayout.CENTER));
		panel_jouer.setLayout(new FlowLayout(FlowLayout.CENTER));
		panel_option.setLayout(new FlowLayout(FlowLayout.CENTER));
		panel_quitter.setLayout(new FlowLayout(FlowLayout.CENTER));
		//Organisation de tout 
		panel_menu.setLayout(new BoxLayout(panel_menu,BoxLayout.Y_AXIS));
		//Le bouton jouer
		bouton_jouer.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				frame.setContentPane(panelMenuChoix);
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setVisible(true);
				
			}
		});
		//Le bouton options
		bouton_option.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("options");
				Options opt = new Options(frame);
				//Options opt = new Options(frame);
			}
		});
		
		//Fermer le frame en appuyant sur le bouton quitter 
		bouton_quitter.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				frame.dispose();
				
			}
		});
		
		
		//Afficher le MENU PRICIPALE dans la fenetre 
		frame.setContentPane(panel_menu);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
}
