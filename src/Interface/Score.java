package Interface;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class Score {
	
	public Score () {
		//Initialisation de la fen�tre
				JFrame frame = new JFrame("TimeLine & CardsLine");
				frame.setSize(925,500);
				// Tableau des score
				Object[][] donnees = {  
						   {"Adel", "2000", "5", 
						      "2"}, 
						   {"Anne-Laure", "2500", "3", 
						      "1"}, 
						} ;
						String[] titreColonnes = { 
						   "Joueur","Score", "Faute",
						   "Classement"}; 
						JTable jTable = new JTable(
						      donnees, titreColonnes);
				
				//Composant de la fen�tre
				JLabel resultat = new JLabel ("R�sultat");
				resultat.setFont(new Font("Serif", Font.BOLD, 30));
				JLabel gagnant = new JLabel ("Le gagnant est :");
				gagnant.setFont(new Font("Serif", Font.PLAIN, 20));
				
				JButton rejouer = new JButton ("Rejouer");
				JButton menuJeu = new JButton ("Menu Jeu");
				JButton quitter = new JButton ("Quitter");
				
				//Panel n�cessaire 
				JPanel panelres = new JPanel();	
				JPanel paneltab = new JPanel();
				JPanel panelbouton = new JPanel();		
				
				panelbouton.setPreferredSize(new Dimension(925, 50));
				
				//Ajout dans les panels
				panelres.add(resultat);
				paneltab.add(new JScrollPane(jTable));
				paneltab.add(gagnant);
				
				panelbouton.add(rejouer);
				panelbouton.add(menuJeu);
				panelbouton.add(quitter);
				
				//Organisation des panels 
				panelres.setLayout(new FlowLayout(FlowLayout.CENTER));
				paneltab.setLayout(new BoxLayout(paneltab, BoxLayout.Y_AXIS));
				panelbouton.setLayout(new GridLayout(1, 4));
				
				//Organisation du frame
				frame.setLayout(new BorderLayout());
				    //On ajoute le bouton au content pane de la JFrame
				    //Au centre
				    frame.getContentPane().add(paneltab, BorderLayout.CENTER);
				    //Au nord
				    frame.getContentPane().add(panelres, BorderLayout.NORTH);
				    //Au sud
				    frame.getContentPane().add(panelbouton, BorderLayout.SOUTH);
				    //� l'ouest
				    frame.getContentPane().add(new JLabel(), BorderLayout.WEST);
				    //� l'est
				    frame.getContentPane().add(new JLabel(), BorderLayout.EAST);
				
				    //Bouton quitter     
				    quitter.addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent arg0) {
							// TODO Auto-generated method stub
							frame.dispose();
						}
					});
				    
				    menuJeu.addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent arg0) {
							// TODO Auto-generated method stub
							frame.dispose();
							Menu c = new Menu ();
						}
					});
				    
				    rejouer.addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent arg0) {
							// TODO Auto-generated method stub
							// A programmer par la suite 
							// Elle renvoie directement vers plateau
						}
					});
				    
				
				frame.setLocationRelativeTo(null);
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setVisible(true);
				
	}

}
