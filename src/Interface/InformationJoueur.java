package Interface;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Calendar;
import java.util.Date;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerListModel;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;



public class InformationJoueur {

	public InformationJoueur (JFrame frame) {
		
		//JCombobox pour le choix du nombre des joueurs + Bot
		 JComboBox <String> comboNbJoueur = new JComboBox <String>();
		 JComboBox <String> comboNbBot = new JComboBox <String>();
		//Taille des JComboBox
		comboNbJoueur.setPreferredSize(new Dimension(50, 25));
		comboNbBot.setPreferredSize(new Dimension(50, 25));
		//Remplir les Jcomobox
		comboNbBot.addItem("0");
		for (Integer i=1; i<9; i++) {
		comboNbJoueur.addItem(i.toString());
		comboNbBot.addItem(i.toString());
		}
		
		//Cach� le JComboBox de BOT 
		
		comboNbBot.setVisible(false);
		
		
		//Declaration des labels
		JLabel labelErreur = new JLabel("Le nombre de joueur n'est pas bon ! De 2 � 8 joueurs !");
		JLabel labelBonjour = new JLabel("BIENVENUE");
		JLabel labelInfo = new JLabel("Information � saisir");
		JLabel labelNbJoueur = new JLabel("Nombre de joueurs ");
		JLabel labelBot = new JLabel("Ajout BOT ");
		JLabel labelNbBot = new JLabel ("Nombre de BOT ");
		//format des label 
		labelBonjour.setFont(new Font("Serif", Font.BOLD, 40));
		labelInfo.setFont(new Font("Serif", Font.BOLD, 25));
		labelNbJoueur.setFont(new Font("Serif", Font.PLAIN, 18));
		labelBot.setFont(new Font("Serif", Font.PLAIN, 18));
		labelNbBot.setFont(new Font("Serif", Font.PLAIN, 18));
		labelErreur.setFont(new Font("Serif", Font.CENTER_BASELINE, 20));
		//Creation bouton valider
		JButton boutonOK = new JButton("VALIDER");
		boutonOK.setPreferredSize(new Dimension(100,50));
		boutonOK.setBounds(10, 50, 70, 40);
		//Bouton commencer
		JButton boutonStart = new JButton("Commencer");
		boutonStart.setPreferredSize(new Dimension(150,70));
		boutonStart.setBackground(Color.GREEN);
		boutonStart.setVisible(false);
		//boutonOK.setBounds(1000,4500,1000,5000);
		//Create the radio buttons
	    JRadioButton ouiBouton = new JRadioButton("OUI");
	    JRadioButton nonBouton = new JRadioButton("NON");
	    nonBouton.setSelected(true);
	   //Group the radio buttons.
	    ButtonGroup group = new ButtonGroup();
	    group.add(ouiBouton);
	    group.add(nonBouton);
	    
		//JPanel n�cessaire 
		JPanel panelBonjour = new JPanel();
		JPanel panelInfo = new JPanel();
		JPanel panelNbJoueur = new JPanel();
		JPanel panelBot = new JPanel();
		JPanel panelNbBot = new JPanel();
		JPanel panelOK = new JPanel();
		JPanel panelTout = new JPanel();
		JPanel panelErreur = new JPanel();
		JPanel panelBoutonStart = new JPanel();
		
		//Ajout des composants
		panelBonjour.add(labelBonjour);
		panelInfo.add(labelInfo);
		panelNbJoueur.add(labelNbJoueur);
		panelNbJoueur.add(comboNbJoueur);
		panelBot.add(labelBot);
		panelBot.add(ouiBouton);
		panelBot.add(nonBouton);
		panelNbBot.add(labelNbBot);
		panelNbBot.add(comboNbBot);
		panelOK.add(boutonOK);
		panelBoutonStart.add(boutonStart);
		panelErreur.add(labelErreur);
		
		
		panelOK.setLayout(new GridLayout(1, 4));
		panelOK.add(new JLabel ());
		panelOK.add(new JLabel ());
		panelOK.add(new JLabel ());
		panelOK.add(new JLabel ());
		panelOK.add(new JLabel ());
		panelOK.add(new JLabel ());
		
	
		panelOK.add(boutonOK);
		//D�sactiver l'erreur par d�faut
		labelErreur.setVisible(false);
		
		//Alignement
		panelBonjour.setLayout(new FlowLayout(FlowLayout.CENTER));
		panelInfo.setLayout(new FlowLayout(FlowLayout.CENTER));
		panelNbJoueur.setLayout(new FlowLayout(FlowLayout.CENTER));
		panelBot.setLayout(new FlowLayout(FlowLayout.CENTER));
		panelNbBot.setLayout(new FlowLayout(FlowLayout.CENTER));
		panelErreur.setLayout(new FlowLayout(FlowLayout.CENTER));
		//panelOK.setLayout(new FlowLayout(FlowLayout.RIGHT));
		panelBoutonStart.setLayout(new FlowLayout(FlowLayout.CENTER));
		
		//Ajout au panelTout
		panelTout.add(panelBonjour);
		panelTout.add(panelInfo);
		panelTout.add(panelErreur);
		panelTout.add(panelNbJoueur);
		panelTout.add(panelBot);
		panelTout.add(panelNbBot);
		panelTout.add(panelBoutonStart);
		panelTout.add(panelOK);
		//Organsation
		//panelTout.setBackground(Color.BLUE);
	//	panelOK.setBackground(Color.RED);
		//boutonOK.setBounds(50, -100, 42, -150);
		//boutonOK.add(Box.createHorizontalGlue());
		panelTout.setLayout(new BoxLayout(panelTout, BoxLayout.Y_AXIS));
		//panelTout.setSize(8000,8000);
		
		
		//ActionListener des bouton 
		// OUI 
		ouiBouton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				comboNbBot.setVisible(true);
				
			}
		});
		//NON
		nonBouton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				comboNbBot.setVisible(false);
				comboNbBot.setSelectedItem("0");
			}
		});
		
		//Bouton VALIDER
		boutonOK.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int nbJoueur = Integer.parseInt((String) comboNbBot.getSelectedItem( ))+ Integer.parseInt((String) comboNbJoueur.getSelectedItem());
				System.out.println(nbJoueur);
				if(( nbJoueur> 1 ) && ( nbJoueur < 9 )) {
					labelErreur.setVisible(false);
					// saisir autant de nom d'age que de joueurs
					//AjoutJoueur joueur = new AjoutJoueur(frame,Integer.parseInt((String) comboNbJoueur.getSelectedItem()));
					int i =1;
					String age;
					String nom;
					//Boucle While pour ecrire les informations relatif au joueur 
					while (i<=Integer.parseInt((String) comboNbJoueur.getSelectedItem())) {
					//D�claration des boite de dialog 
					JOptionPane jop = new JOptionPane(), jop2 = new JOptionPane(),jop3 = new JOptionPane();
				    //Boite de dialog pour le nom avec une contrainte que le nom ne soit pas vide
					do {
					//On stocke le nom du joueur dans la variable nom	
					nom = jop.showInputDialog(null, "Veuillez donner votre nom (Joueur"+i+") !", "Informations joueur !", JOptionPane.QUESTION_MESSAGE);
				    }while (nom.isEmpty()==true);
					// Boite de dialog pour l'age avec gestion d'erreur.
					do {
						//On stocke l'age du joueur dans la variable age		
				     age = jop3.showInputDialog(null, "Veuillez donner votre �ge(Joueur"+i+") !", "Informations joueur !", JOptionPane.QUESTION_MESSAGE);
				    
				   } while (age.isEmpty()==true || ErreurAge.isDigitsOnly(age)==false);
					//Boite de dialog qui r�capitule la saisie
				    jop2.showMessageDialog(null, "Votre nom est :" + nom+"\nVotre �ge est :"+age, "Joueur"+i, JOptionPane.INFORMATION_MESSAGE);
					//compteur de nomnre de joueur qui a saisi ses informations
				    i++;
					}
					//Cacher le bouton valider
					boutonOK.setVisible(false);
					//bloquer les bouton radio et les comboBox pour que le joueur ne puisse pas modifer
					ouiBouton.setEnabled(false);
					nonBouton.setEnabled(false);
					comboNbJoueur.setEnabled(false);
					comboNbBot.setEnabled(false);
					//Afficher le bouton commencer
					boutonStart.setVisible(true);
				}
				else {
					labelErreur.setVisible(true);
				}
				
			}
		});
		
		//Afficher le  TOUT dans la fenetre 
		//Afficher le panelTout dans la frame
		frame.setContentPane(panelTout);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
}